<?php

function seofriendlyURL($urlstring){
	$url = strtolower($urlstring);
	$patterns = $replacements = array();
	$patterns[0] = '/(&amp;|&)/i';
	$replacements[0] = '-and-';
	$patterns[1] = '/,/';
	$replacements[1] = '';
	$patterns[2] = '/[^a-zA-Z01-9]/i';
	$replacements[2] = '-';
	$patterns[3] = '/(-+)/i';
	$replacements[3] = '-';
	$patterns[4] = '/(-$|^-)/i';
	$replacements[4] = '';
	$url = preg_replace($patterns, $replacements, $url);
	return $url;
}

echo "http://test.com/".seofriendlyURL("welcome to my page");

?>
